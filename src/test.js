import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import { Redirect } from "react-router-dom";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import Link from "@material-ui/core/Link";
import firebase from "firebase";
import AddTask from "./addTask";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];
class Album extends React.Component {
  constructor() {
    super();
    this.state = {
      user: undefined,
      list: []
    };
  }

  componentDidMount() {
    if (firebase.auth().currentUser !== null) {
      this.setState({ user: firebase.auth().currentUser });
      let data = [],
        count = 0;
      console.log();
      firebase
        .firestore()
        .collection("tasks")
        .onSnapshot(querySnapshot => {
          querySnapshot.docChanges().forEach(change => {
            if (change.doc.data().userId === firebase.auth().currentUser.uid) {
              if (change.type === "modified" || change.type === "removed") {
                data = data.filter(item => item.id !== change.doc.id);
              }
              if (change.type === "added" || change.type === "modified") {
                data.push({
                  id: change.doc.id,
                  title: change.doc.data().title,
                  status: change.doc.data().status,
                  URL: change.doc.data().imageLocation
                });
              }
              count++;
            }
          });

          this.setState({ count: count, list: data });
        });
    }
  }

  onLogout = () => {
    firebase
      .auth()
      .signOut()
      .then(data => this.props.history.push("/"))
      .catch(err => {
        NotificationManager.console.error(err, "Error", 3000);
        console.log(err);
      });
  };
  onDone = id => {
    firebase
      .firestore()
      .collection("tasks")
      .doc(id)
      .set(
        { status: true },
        {
          merge: true
        }
      )
      .then(data => console.log(data))
      .catch(err => {
        NotificationManager.console.error(err, "Error", 3000);
        console.log(err);
      });
  };
  onDelete = id => {
    firebase
      .firestore()
      .collection("tasks")
      .doc(id)
      .delete()
      .then(data => console.log(data))
      .catch(err => {
        NotificationManager.console.error(err, "Error", 3000);
        console.log(err);
      });
  };
  render() {
    if (firebase.auth().currentUser == null) {
      return <Redirect to={"/"} />;
    } else {
      return (
        <React.Fragment>
          <CssBaseline />
          <AppBar position="relative">
            <Toolbar>
              <Typography variant="h6" color="inherit" noWrap>
                Task's
              </Typography>
            </Toolbar>
          </AppBar>
          <main>
            <div className={"heroContent"}>
              <Container maxWidth="sm">
                <Typography
                  component="h1"
                  variant="h2"
                  align="center"
                  color="textPrimary"
                  gutterBottom
                >
                  List of Tasks
                </Typography>

                <div className={"heroButtons"}>
                  <Grid container spacing={2} justify="center">
                    <Grid item>
                      <AddTask
                        count={this.state.count}
                        user={this.state.user}
                      />
                    </Grid>
                    <Grid item>
                      <Button
                        variant="outlined"
                        onClick={event => this.onLogout()}
                        color="primary"
                      >
                        Logout
                      </Button>
                    </Grid>
                  </Grid>
                </div>
              </Container>
            </div>
            <Container className={"cardGrid"} maxWidth="lg">
              <Grid container spacing={4}>
                {this.state.list.map(card => (
                  <Grid item key={card} xs={12} sm={6} md={3}>
                    <Card style={{ maxWidth: "50vh" }}>
                      <img
                        alt="Task Image"
                        style={{ height: "50vh" }}
                        src={card.URL}
                      />
                      <CardContent>
                        <Typography gutterBottom variant="h5" component="h2">
                          {card.title}
                        </Typography>
                        <Typography
                          variant="body2"
                          color="textSecondary"
                          component="p"
                        >
                          Lizards are a widespread group of squamate reptiles,
                          with over 6,000 species, ranging across all continents
                          except Antarctica
                        </Typography>
                      </CardContent>
                      <CardActions>
                        {card.status === false ? (
                          <Button
                            size="small"
                            onClick={event => {
                              this.onDone(card.id);
                            }}
                            color="primary"
                          >
                            Done
                          </Button>
                        ) : (
                          ""
                        )}
                        <Button
                          onClick={event => this.onDelete(card.id)}
                          size="small"
                          color="primary"
                        >
                          Delete
                        </Button>
                      </CardActions>
                    </Card>
                  </Grid>
                ))}
              </Grid>
            </Container>
          </main>
          <NotificationContainer />
        </React.Fragment>
      );
    }
  }
}
export default Album;
