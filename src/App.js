import React from "react";
import { Redirect, BrowserRouter, Route, Switch } from "react-router-dom";
import "./App.css";
import SignIn from "./signin";
import test from "./test";
import firebase from "./firebase";

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      loading: true
    };
  }

  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path={"/"} exact component={SignIn} />
          <Route path={"/task"} component={test} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
