import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import firebase from "firebase";
import React from "react";
import Grid from "@material-ui/core/Grid";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
import imageCompression from "browser-image-compression";
const objToday = new Date();

class Withdrawl extends React.Component {
  state = {
    open: false,
    Button: true,
    title: undefined,
    compressedFile: undefined
  };

  componentDidUpdate() {}

  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  handleRequestClose = () => {
    this.setState({
      Button: true,
      title: undefined,
      compressedFile: undefined,
      compressedFileURL: undefined
    });
  };
  handleRequestClose1 = () => {
    this.setState({
      open: false
    });
  };

  Submit = () => {
    this.handleRequestClose1();
    var uploadTask = firebase
      .storage()
      .ref()
      .child(
        this.props.user.uid +
          "/task-" +
          this.props.count +
          this.state.compressedFile.name
      )
      .put(this.state.compressedFile);

    uploadTask.on(
      firebase.storage.TaskEvent.STATE_CHANGED,

      () => {
        // Upload completed successfully, now we can get the download URL
        uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
          console.log(this.state);
          firebase
            .firestore()
            .collection("tasks")
            .add({
              title: this.state.title,
              imageLocation: downloadURL,
              status: false,
              userId: this.props.user.uid
            })
            .then(data => {
              this.handleRequestClose();
              console.log(data);
            })
            .catch(err => {
              NotificationManager.console.error(err.message, "Error", 3000);
              console.log(err);
            });
        });
      }
    );
  };
  onChange = event => {
    this.setState({ title: event.target.value });
    if (this.state.compressedFile !== undefined) {
      this.setState({ Button: false });
    } else {
      this.setState({ Button: true });
    }
  };
  handleImageUpload = event => {
    const imageFile = event.target.files[0];

    this.setState({
      compressedFile: imageFile,
      compressedFileURL: URL.createObjectURL(imageFile)
    });
    // await uploadToServer(compressedFile); // write your own logic

    if (this.state.title !== undefined) {
      this.setState({ Button: false });
    } else {
      this.setState({ Button: true });
    }
  };
  render() {
    return (
      <div>
        <Button
          variant="contained"
          color="primary"
          className="btn btn-primary jr-btn-rounded jr-btn"
          onClick={this.handleClickOpen}
        >
          Add Task
        </Button>
        <Dialog
          open={this.state.open}
          maxWidth="sm"
          fullWidth
          onClose={this.handleRequestClose}
        >
          <DialogTitle> Add Task to List </DialogTitle>{" "}
          <DialogContent>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <TextField
                  margin="dense"
                  onChange={event => {
                    this.handleChange(event);
                  }}
                  name="title"
                  label="Title"
                  type="String"
                  value={this.state.title}
                  onChange={event => {
                    this.onChange(event);
                  }}
                  fullWidth
                />
              </Grid>
              <Grid item xs={12}>
                <input
                  type="file"
                  accept="image/*"
                  onChange={event => this.handleImageUpload(event)}
                />
              </Grid>
              <Grid item xs={12}>
                <img
                  alt="task image"
                  width="150"
                  src={this.state.compressedFileURL}
                />
              </Grid>
            </Grid>
          </DialogContent>
          <DialogActions>
            <Button
              variant="contained"
              color="primary"
              disabled={this.state.Button}
              onClick={evevnt => this.Submit()}
              className="btn btn-primary jr-btn-rounded jr-btn"
            >
              Add
            </Button>
          </DialogActions>
        </Dialog>
        <NotificationContainer />
      </div>
    );
  }
}

export default Withdrawl;
