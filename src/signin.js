import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Link from "@material-ui/core/Link";
import { Redirect } from "react-router-dom";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";
import firebase from "firebase";
import ReactPhoneInput from "react-phone-input-2";
import "react-phone-input-2/dist/style.css";
import {
  NotificationContainer,
  NotificationManager
} from "react-notifications";
// var phoneNumber = "+919535521455";
// var testVerificationCode = "123456";

class SignIn extends React.Component {
  constructor() {
    super();
    this.state = {
      verifyCode: false,
      phoneNumber: "",
      ResultConfirmation: undefined,
      loading: true,
      testVerificationCode: undefined
    };
    console.log(firebase.auth().currentUser);
  }
  componentDidMount() {
    setTimeout(() => this.setState({ loading: false }), 2500);
    window.recaptchaVerifier = new firebase.auth.RecaptchaVerifier(
      this.recaptcha,
      {
        size: "normal",
        callback: function(response) {
          // reCAPTCHA solved, allow signInWithPhoneNumber.
          // ...
        },
        "expired-callback": function() {
          // Response expired. Ask user to solve reCAPTCHA again.
          // ...
        }
      }
    );
    window.recaptchaVerifier.render().then(function(widgetId) {
      window.recaptchaWidgetId = widgetId;
    });
  }

  handleChange = event => {
    const key = event.target.name;
    this.setState({ [key]: event.target.value });
  };
  phoneNumber = event => {
    this.setState({ phoneNumber: event });
  };

  sendCode = () => {
    var appVerifier = window.recaptchaVerifier;
    firebase
      .auth()
      .signInWithPhoneNumber(this.state.phoneNumber, appVerifier)
      .then(confirmationResult => {
        // confirmationResult can resolve with the whitelisted testVerificationCode above.
        NotificationManager.info("OTP has been sent", "OTP send", 3000);
        this.setState({ verifyCode: true });
        this.setState({ Resultconfirmation: confirmationResult });
      })
      .catch(error => {
        // Error; SMS not sent
        // ...
        NotificationManager.error(error.message, "Error", 3000);
        console.log(error);
      });
  };
  signin = () => {
    if (this.state.testVerificationCode === undefined) {
      NotificationManager.error("Please Enter OTP", "OTP not Found", 3000);
    } else {
      var credential = firebase.auth.PhoneAuthProvider.credential(
        this.state.Resultconfirmation.verificationId,
        this.state.testVerificationCode
      );
      firebase
        .auth()
        .signInWithCredential(credential)
        .then(data => {
          this.props.history.push("/task");
          console.log(data);
        })
        .catch(err => {
          NotificationManager.error(err.message, "err", 3000);
          this.props.history.push("/");
          console.log(err);
        });
    }
  };
  render() {
    if (firebase.auth().currentUser !== null) {
      return <Redirect to={"/task"} />;
    }
    return (
      <div>
        <Container component="main" maxWidth="xs">
          <div className={"paper"}>
            <Typography component="h1" variant="h5">
              Sign in/Sign up
            </Typography>
            <div className={"form"}>
              {this.state.verifyCode === false ? (
                <div>
                  <ReactPhoneInput
                    defaultCountry={"in"}
                    value={this.state.phoneNumber}
                    onChange={event => this.phoneNumber(event)}
                    name="phoneNumber"
                  />
                  <div ref={ref => (this.recaptcha = ref)}></div>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={event => this.sendCode()}
                  >
                    Send OTP
                  </Button>
                </div>
              ) : (
                <div>
                  <h3>Phone Number-{this.state.phoneNumber}</h3>
                  <TextField
                    variant="outlined"
                    margin="normal"
                    required
                    fullWidth
                    value={this.state.testVerificationCode}
                    onChange={event => this.handleChange(event)}
                    name="testVerificationCode"
                    label="Verification Code"
                  />
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    onClick={event => this.signin()}
                  >
                    Sign In
                  </Button>
                  <Grid container>
                    <Grid item xs>
                      <Button onClick={event => this.sendCode()}>
                        Resend code
                      </Button>
                    </Grid>
                    <Grid item xs>
                      <Button
                        onClick={event => {
                          this.setState({
                            phoneNumber: undefined,
                            verifyCode: false
                          });
                          window.recaptchaVerifier.render().then(widgetId => {
                            window.recaptchaVerifier.reset(widgetId);
                          });
                        }}
                      >
                        Change Phone Number
                      </Button>
                    </Grid>
                  </Grid>
                </div>
              )}
            </div>
          </div>
        </Container>
        <NotificationContainer />
      </div>
    );
  }
}
export default SignIn;
