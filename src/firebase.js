import * as firebase from "firebase";
const config = {
  apiKey: "AIzaSyAagLPTYa45xxL3lJbcTPAIV80dVByw1I4",
  authDomain: "task-manager-46493.firebaseapp.com",
  databaseURL: "https://task-manager-46493.firebaseio.com",
  projectId: "task-manager-46493",
  storageBucket: "task-manager-46493.appspot.com",
  messagingSenderId: "1039308007729",
  appId: "1:1039308007729:web:0d859a9ab16ab8d9e72810"
};
firebase.initializeApp(config);

firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION);
export default firebase;
